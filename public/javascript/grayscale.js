(function($) {
  "use strict"; 

  
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 70)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 100
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

})(jQuery); // End of use strict

var music = document.getElementById('music'); // id for audio element
      var duration = music.duration; // Duration of audio clip, calculated here for embedding purposes
      var pButton = document.getElementById('pButton'); // play button
      var playhead = document.getElementById('playhead'); // playhead
      var timeline = document.getElementById('timeline'); // timeline

      // timeline width adjusted for playhead
      var timelineWidth = timeline.offsetWidth - playhead.offsetWidth;

      // play button event listenter
      pButton.addEventListener("click", play);

      // timeupdate event listener
      music.addEventListener("timeupdate", timeUpdate, false);

      // makes timeline clickable
      timeline.addEventListener("click", function (event) {
          moveplayhead(event);
          music.currentTime = duration * clickPercent(event);
      }, false);

      // returns click as decimal (.77) of the total timelineWidth
      function clickPercent(event) {
          return (event.clientX - getPosition(timeline)) / timelineWidth;
      }

      // makes playhead draggable
      playhead.addEventListener('mousedown', mouseDown, false);
      window.addEventListener('mouseup', mouseUp, false);

      // Boolean value so that audio position is updated only when the playhead is released
      var onplayhead = false;

      // mouseDown EventListener
      function mouseDown() {
          onplayhead = true;
          window.addEventListener('mousemove', moveplayhead, true);
          music.removeEventListener('timeupdate', timeUpdate, false);
      }

      // mouseUp EventListener
      // getting input from all mouse clicks
      function mouseUp(event) {
          if (onplayhead == true) {
              moveplayhead(event);
              window.removeEventListener('mousemove', moveplayhead, true);
              // change current time
              music.currentTime = duration * clickPercent(event);
              music.addEventListener('timeupdate', timeUpdate, false);
          }
          onplayhead = false;
      }
      // mousemove EventListener
      // Moves playhead as user drags
      function moveplayhead(event) {
          var newMargLeft = event.clientX - getPosition(timeline);

          if (newMargLeft >= 0 && newMargLeft <= timelineWidth) {
              playhead.style.marginLeft = newMargLeft + "px";
          }
          if (newMargLeft < 0) {
              playhead.style.marginLeft = "0px";
          }
          if (newMargLeft > timelineWidth) {
              playhead.style.marginLeft = timelineWidth + "px";
          }
      }

      // timeUpdate
      // Synchronizes playhead position with current point in audio
      function timeUpdate() {
          var playPercent = timelineWidth * (music.currentTime / duration);
          playhead.style.marginLeft = playPercent + "px";
          if (music.currentTime == duration) {
              pButton.className = "";
              pButton.className = "fas fa-play";
          }
      }

      //Play and Pause
      function play() {
          // start music
          if (music.paused) {
              music.play();
              // remove play, add pause
              pButton.className = "";
              pButton.className = "fas fa-pause";
          } else { // pause music
              music.pause();
              // remove pause, add play
              pButton.className = "";
              pButton.className = "fas fa-play";
          }
      }

      // Gets audio file duration
      music.addEventListener("canplaythrough", function () {
          duration = music.duration;
      }, false);

      // getPosition
      // Returns elements left position relative to top-left of viewport
      function getPosition(el) {
          return el.getBoundingClientRect().left;
      }
      
